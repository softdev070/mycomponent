/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wimonsiri.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author Acer
 */
public class Product {

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
    private int id;
    private String name;
    private double price;
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, " hot Americano", 50, "1.png"));
        list.add(new Product(2, "hot Cappuccino", 45, "2.jpg"));
        list.add(new Product(3, "hot Cocoa", 40, "3.jpg"));
        list.add(new Product(4, "hot Milk", 40, "4.jpg"));
        list.add(new Product(5, "iced Latte", 50, "5.jpg"));
        list.add(new Product(6, "iced Cappuchino", 55, "6.jpg"));
        list.add(new Product(7, "iced Cocoa", 50, "7.jpg"));
        list.add(new Product(8, "iced Eppresso", 50, "8.jpg"));
        list.add(new Product(9, "iced Milk", 45, "9.jpg"));
        list.add(new Product(10, "iced Thaitea", 45, "10.jpg"));
        return list;

    }

}
